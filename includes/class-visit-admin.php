<?php
/**
 * Visitizer Admin Class
 *
 * Adds functionality on the back-end so WP admin is more user-friendly
 *
 * @package VS_Visitzer
 * @since 0.1
 */

/**
 * Post Type Class
 *
 * Adds appropriate columns and messages so that the logs can be viewed in
 * the WP admin
 *
 * @since 0.1
 */
class Visit_Admin {

	/**
	 * Post type name
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $post_type = 'visit_log';

	/**
	 * Class instance
	 *
	 * @access protected
	 * @var object
	 * @since 0.1
	 */
	protected static $instance = null;


	/**
	 * Admin initialization
	 *
	 * Adds all functionality for viewing the Visit Log post type in WP admin
	 *
	 * @since 0.1
	 */
	public function __construct() {
		add_filter( 'manage_edit-visit_log_columns', array( $this, 'add_columns_to_post_type' ) );
		add_action( 'manage_visit_log_posts_custom_column', array( $this, 'manage_post_type_columns' ), 10, 2 );
		add_filter( 'list_table_primary_column', array( $this, 'edit_primary_table' ), 10, 2 );
		add_filter( 'post_row_actions', array( $this, 'edit_visit_log_actions' ), 10, 2 );
		add_filter( 'manage_edit-visit_log_sortable_columns', array( $this, 'add_sortable_columns_to_post_type' ) );
		add_action( 'load-edit.php', array( $this, 'manage_sortable_columns' ) );
		add_filter( 'bulk_post_updated_messages', array( $this, 'add_visit_log_messages' ), 10, 2 );
		add_filter( 'bulk_actions-edit-visit_log', '__return_empty_array' );
		add_filter( 'views_edit-visit_log', '__return_empty_array' );
	}


	/**
	 * Stores reference to itself
	 *
	 * @since 0.1
	 * @return object instance of class
	 */
	public static function init() {
		if ( null === self::$instance ) {
			self:$instance = new Visit_Admin;
		}

		return self::$instance;
	}

	/**
	 * Add columns to Visit Post Type
	 *
	 * The default columns for the post type are not sufficient enough
	 * for viewing. This allows users to see all of the information for
	 * a post type without needing additional screens.
	 *
	 * @since 0.1
	 * @param array $columns Default columns.
	 */
	public static function add_columns_to_post_type( $columns ) {
		$columns = array(
			'visitor_email' 	=> __( 'Visitor Email', 'visitizer' ),
			'visitor_name' 		=> __( 'Visitor Name', 'visitizer' ),
			'visiting' 				=> __( 'Visited', 'visitizer' ),
			'desk' 						=> __( 'Visited Desk', 'visitizer' ),
			'time' 						=> __( 'Time of Visit', 'visitizer' ),
		);

		return $columns;
	}

	/**
	 * Adds a list of columns we want to make sortable
	 *
	 * @param array $columns List of columns.
	 */
	public function add_sortable_columns_to_post_type( $columns ) {
		$columns['visitor_email'] = 'visitor_email';
		$columns['time'] 					= 'time';
		$columns['desk'] 					= 'desk';

		return $columns;
	}

	/**
	 * Just pass the hook down one more level
	 */
	public function manage_sortable_columns() {
		add_filter( 'request', array( $this, 'add_sort_functionality' ) );
	}

	/**
	 * Filters query variables based on sortable columns
	 *
	 * @param array $vars List of edit screen variables.
	 */
	public static function add_sort_functionality( $vars ) {
		if ( isset( $vars['post_type'] ) && self::$post_type === $vars['post_type'] ) {

			if ( isset( $vars['orderby'] ) && 'desk' === $vars['orderby'] ) {
				$vars = array_merge( $vars, array(
					'orderby' => 'author',
				) );
			}

			if ( isset( $vars['orderby'] ) && 'visitor_email' === $vars['orderby'] ) {
				$vars = array_merge( $vars, array(
					'orderby' => 'title',
				) );
			}
		}

		return $vars;
	}

	/**
	 * Functionality for post type columns
	 *
	 * Outputs the proper information in each column.
	 * If statement is used instead of switch/case for clarity.
	 *
	 * @since 0.1
	 * @global object $post    Full post object.
	 * @param  string $column  Name of current column.
	 * @param  int    $post_id    ID of post.
	 */
	public static function manage_post_type_columns( $column, $post_id ) {
		global $post;

		if ( 'visitor_email' === $column ) {
			printf( '<strong><a href="mailto:%s">%s</a></strong>', esc_html( get_the_title() ), esc_html( get_the_title() ) );
		}

		if ( 'visitor_name' === $column ) {
			echo esc_html( get_the_content() );
		}

		if ( 'visiting' === $column ) {
			echo esc_html( get_the_excerpt() );
		}

		if ( 'desk' === $column ) {
			echo absint( $post->post_author );
		}

		if ( 'time' === $column ) {
			printf( '%s <br />@ %s', esc_html( get_the_date() ), esc_html( get_the_time() ) );
		}

	}

	/**
	 * For the visit log post type, cahnge the primary table to email
	 *
	 * This is important, because responsive functionality is dependent on the
	 * existence of a primary table
	 *
	 * @param  string $default  Default primary.
	 * @param  string $context 	What screen we are on.
	 * @return string          	The table to use as primary
	 */
	public static function edit_primary_table( $default, $context ) {
		if ( 'edit-visit_log' === $context ) {
			return 'visitor_email';
		} else {
			return $default;
		}
	}

	/**
	 * For the visit log, leave the option to delete the post,
	 * but get rid of other row actions
	 *
	 * @param  array  $actions   List of actions.
	 * @param  object $post    	Full post object.
	 * @return array            New row actions
	 */
	public function edit_visit_log_actions( $actions, $post ) {
		if ( self::$post_type === $post->post_type ) {

			// Not permanently deleting, because we'd like to leave "Undo" available.
			$delete_link = esc_url( get_delete_post_link( $post->ID ) );
			return array(
				'trash' => sprintf( '<a href="%s">%s</a>', $delete_link, 'Delete' ),
			);
		}

		return $actions;
	}

	/**
	 * Add visit log post type messages
	 *
	 * This makes messages more understandable when a user deletes or
	 * undos a delete on the back-end
	 *
	 * @param array $bulk_messages  List of messages for post types.
	 * @param int   $bulk_counts      Number of posts affected.
	 * @return array                Full set of post messages
	 */
	public function add_visit_log_messages( $bulk_messages, $bulk_counts ) {
		$new_messages[ self::$post_type ] = array(
			'trashed' 	=> esc_html( _n( '%s log has been deleted.', '%s logs have been deleted.', absint( $bulk_counts['trashed'] ), 'visitizer' ) ),
			'untrashed' => esc_html( _n( '%s log has been restored.', '%s logs have been restored.', absint( $bulk_counts['untrashed'] ), 'visitizer' ) )
		);

		return array_merge( $bulk_messages, $new_messages );
	}
}
