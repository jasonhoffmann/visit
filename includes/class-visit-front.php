<?php
/**
 * Frontend Definitions and Functions
 *
 * Adds the visit sign in form and all related functions
 *
 * @package VS_Visitzer
 * @since 0.1
 */

/**
 * Frontend Visit Log class
 *
 * Adds the form for the front-end, viewable at /visit/
 * Validates form on submission and adds a new post, with post type
 * Visit Log, upon submission
 *
 * @since 0.1
 */
class Visit_Front {

	/**
	 * Instance of class
	 *
	 * @access protected
	 * @var object
	 * @since 0.1
	 */
	protected static $instance = null;

	/**
	 * Name of AJAX action
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $action = 'visitor_signin';

	/**
	 * Name of Nonce
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $noncename = 'vt_visitor_form';

	/**
	 * Name of post type
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $post_type = 'visit_log';

	/**
	 * Localized messages array
	 * Added on construct
	 *
	 * @access protected
	 * @var array
	 * @since 0.1
	 */
	protected static $messages = array();

	/**
	 * Holds coworkers from options
	 *
	 * @access protected
	 * @var array
	 * @since 0.1
	 */
	protected static $coworkers = null;

	/**
	 * Class initialization
	 *
	 * Contains reference to class
	 *
	 * @since 0.1
	 * @return object class instance
	 */
	public static function init() {
		if ( null === self::$instance ) {
			self:$instance = new Visit_Front;
		}

		return self::$instance;
	}

	/**
	 * WordPress hooks and filters
	 *
	 * Adds the hooks necessary for front-end form and AJAX submission
	 * Kicks off function to register class variables
	 *
	 * @since 0.1
	 */
	public function __construct() {
		add_shortcode( 'visitor_signin', array( $this, 'visitor_form_shortcode' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'add_visitor_scripts' ) );

		add_action( 'wp_ajax_visitor_signin', array( $this, 'process_ajax_request' ) );
		add_action( 'wp_ajax_nopriv_visitor_signin', array( $this, 'process_ajax_request' ) );

		$this->register_messages();
	}

	/**
	 * Add data for messages and options variables
	 *
	 * @since 0.1
	 */
	private function register_messages() {
		$invalid_email = __( 'Please Enter a Valid Email Address', 'visitizer' );
		$empty_name = __( 'Please Enter Your Name', 'visitizer' );
		$empty_visiting = __( 'Please Enter The Name of the Person You Are Visiting', 'visitizer' );
		$invalid_visiting = __( 'I\'m sorry, that person does not work here.', 'visitizer' );
		$server_issue = __( 'Sorry, we are having an issue connecting to the server.', 'visitizer' );

		self::$messages = array(
			'invalid_email' => $invalid_email,
			'empty_name' => $empty_name,
			'empty_visiting' => $empty_visiting,
			'invalid_visiting' => $invalid_visiting,
			'server_issue' => $server_issue,
		);
	}

	/**
	 * Get list of coworkers from the options table and
	 * store it in a class variable
	 *
	 * @since 0.1
	 * @return array Full option data
	 */
	public static function coworkers_list() {
		if ( null === self::$coworkers ) {
			$coworkers = get_option( 'vt_coworker_list' );
			if( is_array( $coworkers ) ) {
				self::$coworkers = $coworkers;
			} else {
				self::$coworkers = '';
			}
		}

		return self::$coworkers;
	}


	/**
	 * Register scripts and styles
	 *
	 * Adds script which processes form using AJAX and
	 * style which adds some basic, responsive CSS
	 *
	 * @since 0.1
	 */
	public function add_visitor_scripts() {
		wp_register_script( 'visitor-log', VT__PLUGIN_URL . 'assets/js/scripts/process_form.js', array( 'jquery' ), '0.1', true );
		wp_localize_script( 'visitor-log', 'VT', array(
			'ajaxurl' => admin_url( '/admin-ajax.php' ),
			'messages' => self::$messages,
		));
		wp_enqueue_script( 'visitor-log' );

		wp_enqueue_style( 'visitor_log', VT__PLUGIN_URL . 'assets/css/visitor_log.css', array(), '0.1' );
	}

	/**
	 * Handler for shortcode
	 *
	 * Simply passes the buck down to the render form function
	 *
	 * @see render_form
	 *
	 * @since 0.1
	 * @param  array $atts Attributes for shortcode.
	 */
	public function visitor_form_shortcode( $atts ) {
		$form = $this->render_form();
		return $form;
	}

	/**
	 * HTML for form
	 *
	 * Includes nonce field and HTML for errors and success messages
	 *
	 * @since 0.1
	 */
	private function render_form() {
		$coworkers = self::coworkers_list();
		$output = '';

		if ( $coworkers && ! empty( $coworkers ) ) {
			ob_start();
			?>
			<ul id="vt_errors" class="vt_errors vt_alert" style="display: none;"></ul>
			<form id="visit_signup_form" class="vt_form form" method="POST">
		  <div class="vt_name">
			  <label for="vt_name_input"><?php esc_attr_e( 'Name', 'visitizer' ); ?></label>
			  <input type="text" id="vt_name_input" name="vt_name_input" class="vt_name_input" />
		  </div>
		 			
		  <div class="vt_email">
			  <label for="vt_email_input"><?php esc_attr_e( 'Email', 'visitizer' ); ?></label>
			  <input type="email" id="vt_email_input" name="vt_email_input" class="vt_email_input" />
		  </div>
		 			
		  <div class="vt_visiting">
			  <label for="vt_visiting_input"><?php esc_attr_e( 'Who Are You Visiting?', 'visitizer' ); ?></label>
			  <select name="vt_visiting_input" id="vt_visiting_input">
				<?php foreach ( $coworkers as $name => $desk ) { ?>
		 					<option value="<?php echo esc_attr( $name ); ?>">
		 						<?php echo esc_attr( $name ); ?>
		 					</option>
		 				<?php } ?>
			  </select>
		  </div>
		 			
			<?php wp_nonce_field( self::$action, self::$noncename ); ?>
		 			
		  <input class="vt_submit" type="submit" value="<?php esc_attr_e( 'Sign In', 'visitizer' ); ?>" />
			</form>
		 		
			<div id="vt_success" class="vt_success vt_alert" style="display:none;">
		  <p>Thanks for signing in!</p>
		  <p>We hope you enjoy your visit with <span class="js-vt_visiting"></span></p>
		  <p><em>Please proceed to desk <span class="js-vt_desk"></span></em></p>
			</div>
			<?php
			$output .= ob_get_clean();
		} else {
			$text = esc_attr__( 'Sorry, there is no one to visit', 'visitizer' );
			$output .= sprintf( '<p class="vt_errors vt_alert">%s</p>', $text );
		}

		return $output;
	}

	/**
	 * Handle AJAX submission from front-end
	 *
	 * Sanitizes inputs as they come in, compares the list against the
	 * full list of coworkers for a valid entry, and if everything passes,
	 * adds a new post to the Visit Log post type and passes data back to
	 * front-end.
	 *
	 * @see check_for_errors
	 * @see check_for_repeat
	 *
	 * @since 0.1
	 */
	public function process_ajax_request() {
		$nonce = isset( $_POST['nonce'] ); // Input var okay.

		if ( $nonce && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['nonce'] ) ), self::$action ) ) { // Input var okay.

			$email = isset( $_POST['email'] ) ? sanitize_email( wp_unslash( $_POST['email'] ) ) : ''; // Input var okay.

			$name = isset( $_POST['name'] ) ? sanitize_text_field( wp_unslash( $_POST['name'] ) ) : ''; // Input var okay.

			$visiting = isset( $_POST['visiting'] ) ? sanitize_text_field( wp_unslash( $_POST['visiting'] ) ) : ''; // Input var okay.

			$data = array(
				'email' => $email,
				'name' => $name,
				'visiting' => $visiting,
			);

			$this->check_for_errors( $data );

			$this->check_for_repeat( $data );

			$coworkers = self::coworkers_list();

			$desk = isset( $coworkers[ $visiting ] ) ? $coworkers[ $visiting ] : '';
			$visit_data = array(
				'post_type' 		=> self::$post_type,
				'post_status' 	=> 'publish',
				'post_title' 		=> $email,
				'post_content' 	=> $name,
				'post_excerpt' 	=> $visiting,
				'post_author' 	=> $desk,
			);

			$visit_id = wp_insert_post( $visit_data );

			$data['desk'] = $desk;
			$data['next_nonce'] = wp_create_nonce( self::$action );

			// Add action for other plugins to hook into.
			do_action( 'visitor_added', $data, $visit_id );

			echo wp_json_encode( $data );
		}

		wp_die();
	}

	/**
	 * Checks data set for invalid or empty entries
	 *
	 * @param  array $data Full data from request, already sanitized.
	 * @return boolean       True if passes tests
	 */
	private function check_for_errors( $data ) {
		$error = new WP_Error();

		if ( '' === $data['email'] ) {
			$error->add( 'invalid_email',  self::$messages['invalid_email'] );
		}

		if ( '' === $data['name'] ) {
			$error->add( 'empty_name', self::$messages['empty_name'] );
		}

		if ( '' === $data['visiting'] ) {
			$error->add( 'empty_visiting', self::$messages['empty_visiting'] );
		}

		// Only if there are no other errors, check for invalid coworker.
		$coworkers = self::coworkers_list();
		if ( is_array( $coworkers ) && ! $error->get_error_codes() ) {
			if ( ! array_key_exists( $data['visiting'], $coworkers ) ) {
				$error->add( 'invalid_visiting', self::$messages['invalid_visiting'] );
			}
		}

		$this->send_errors( $error );

		return true;
	}

	/**
	 * Check for repeats
	 *
	 * See if a user has already signed in today, by querying posts from
	 * today by title (email)
	 *
	 * @since 0.1
	 * @param  array $data  Full data from POST request.
	 * @return boolean      True if no errors
	 */
	private function check_for_repeat( $data ) {
		$today = getdate();
		$repeat = new WP_Query( array(
			'post_type' 	=> self::$post_type,
			'title' 			=> $data['email'],
			'year'			 	=> $today['year'],
			'monthnum' 		=> $today['mon'],
			'day' 				=> $today['mday'],
		) );

		if ( $repeat->have_posts() ) {
			$error = new WP_Error();
			$error->add( 'duplicate', 'You have already visited once today' );

			$this->send_errors( $error );
		}
		return true;
	}

	/**
	 * Process error object and send back to front-end
	 *
	 * @since 0.1
	 * @param  object $error WP_Error instance.
	 */
	private function send_errors( $error ) {
		if ( $error->get_error_codes() ) {
			$errors = array(
				'status' => 'error',
				'errors' => $error->get_error_messages(),
			);
			echo wp_json_encode( $errors );
			wp_die();
		}
	}

}
