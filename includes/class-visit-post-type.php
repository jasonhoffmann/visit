<?php
/**
 * Visitizer Post Type Class
 *
 * Register and initialize any functions related to setting up
 * the Visit Log Post Type
 *
 * @package VS_Visitzer
 * @since 0.1
 */

/**
 * Post Type Class
 *
 * Registers the main visit log post type
 *
 * @since 0.1
 */
class Visit_Post_Type {

	/**
	 * Post type name
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $slug = 'visit_log';

	/**
	 * Class instance
	 *
	 * @access protected
	 * @var object
	 * @since 0.1
	 */
	protected static $instance = null;


	/**
	 * Post Type initialization
	 *
	 * Sets up WordPress hooks for registering the Visit Log
	 * Post type
	 *
	 * @since 0.1
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'add_visit_post_type' ) );
	}


	/**
	 * Stores reference to itself
	 *
	 * @since 0.1
	 * @return object instance of class
	 */
	public static function init() {
		if ( null === self::$instance ) {
			self:$instance = new Visit_Post_Type;
		}

		return self::$instance;
	}

	/**
	 * Registers the Visit Log post type
	 *
	 * @since 0.1
	 */
	public static function add_visit_post_type() {
		$labels = array(
			'name'                  => _x( 'Visits', 'Post Type General Name', 'visitizer' ),
			'singular_name'         => _x( 'Visit', 'Post Type Singular Name', 'visitizer' ),
			'menu_name'             => __( 'Visit Log', 'visitizer' ),
			'name_admin_bar'        => __( 'Visits', 'visitizer' ),
			'archives'              => __( 'Visitors', 'visitizer' ),
			'parent_item_colon'     => __( 'Visit:', 'visitizer' ),
			'all_items'             => __( 'All Visits', 'visitizer' ),
			'view_item'             => __( 'View Visit', 'visitizer' ),
			'search_items'          => __( 'Search Visits', 'visitizer' ),
			'not_found'             => __( 'No Visitor Logs', 'visitizer' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'visitizer' ),
		);
		$args = array(
			'label'                 => __( 'Visit', 'visitizer' ),
			'description'           => __( 'Visitor check-in logs', 'visitizer' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'excerpt' ),
			'menu_icon'							=> 'dashicons-editor-ul',
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => false,
			'rewrite'               => false,
			'capability_type'       => 'post',
			'capabilities' 					=> array( 'create_posts' => false ),
			'map_meta_cap' 					=> true,
		);

		register_post_type( self::$slug, $args );
	}

}
