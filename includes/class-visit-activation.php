<?php
/**
 * Activation Hook
 *
 * A little more complicated then it sounds, since we need to create
 * a new page viewable at /visit/ and add a list of coworkers to options.
 *
 * @since 0.1
 * @package VS_Visitzer
 */

/**
 * Simple class for activation
 *
 * Really just one function that kicks everything off.
 *
 * @since 0.1
 */
class VisitActivation {

	/**
	 * Coworkers option name
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $option = 'vt_coworker_list';

	/**
	 * Source URL for coworkers
	 *
	 * @access protected
	 * @var string
	 * @since 0.1
	 */
	protected static $source = 'https://gist.githubusercontent.com/jjeaton/21f04d41287119926eb4/raw/4121417bda0860f662d471d1d22b934a0af56eca/coworkers.json';


	/**
	 * Performed on plugin activation
	 *
	 * Creates a new page with the proper shortcode,
	 * and grabs a list of coworkers from Gist, and then converts it
	 * to a WordPress option
	 *
	 * @since 0.1
	 */
	public static function activate() {

		/**
		 * Delete any existing pages named visit
		 *
		 * This is a bit aggressive, but assuming we need to force
		 * /visit/ to work as promised
		 *
		 * @var WP_Query
		 */
		$existing_page = new WP_Query( array(
			'name' => 'visit',
			'post_type' => 'page',
		) );
		foreach ( $existing_page->posts as $page ) {
			wp_delete_post( $page->ID, true );
		}

		$visit_page = array(
			'post_title' 		=> 'Visit',
			'post_type' 		=> 'page',
			'post_status' 	=> 'publish',
			'post_content' 	=> '[visitor_signin]',
		);
		wp_insert_post( $visit_page );

		$coworkers_gist = wp_safe_remote_get( self::$source );
		$coworkers = json_decode( wp_remote_retrieve_body( $coworkers_gist ) );

		if ( isset( $coworkers ) && is_array( $coworkers ) && ! empty( $coworkers ) ) {
			// Change the data into a hashmap for easy access.
			$coworkers_data = array();
			foreach ( $coworkers as $coworker ) {
				$coworkers_data[ $coworker->name ] = $coworker->desk;
			}
		} else {
			// Make this an empty string so we can detect on output.
			$coworkers_data = '';
		}

		update_option( self::$option, $coworkers_data, false );
	}

}
