var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');

gulp.task('sass', function() {
  return gulp.src('./assets/css/sass/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('scripts', function() {
  return gulp.src('./assets/js/scripts/**/*.js')
    .pipe(concat('visitor-log.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/js'))
})

gulp.task('watch', function() {
  gulp.watch('./assets/css/sass/**/*.scss', ['sass']);
  gulp.watch('./assets/js/scripts/**/*.js', ['scripts']);
});

gulp.task('lint', function() {
  return gulp.src('./assets/js/scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
})

gulp.task('default', ['sass', 'scripts', 'lint', 'watch']);