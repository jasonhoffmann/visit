## Visitizer
### Log users in to Springfield!

Visitizer allows you to easily set up a visit log system that allows users to sign in when visiting a desk. Logs can easily be viewed and sorted, and visitors can only sign in once per day.

To use:
- Download this plugin.
- Upload it to WordPress.
- Visit yoursite.com/visit/ to sign in.

### Notes (in no particular order)
- I chose to set up the /visit/ endpoint by creating a page and adding a shortcode on creation. There are two other ways I can think to do this. First would be to hook into the Visit Log post type archives template and provide our own. The other would be to use add_rewrite_rule or a completely custom routing solution. I chose to create the page because this allows a client to easily edit the page after creation, adding text before or after the form, or changing the URL. In that respect, it's the most flexible option.
- The front-end processing of the form is done in a simple Javascript object. Because cross-browser AJAX is a bit difficult, the script uses jQuery to submit this request, and also relies on jQuery for certain DOM related manipulations. However, removing a reliance on jQuery would be fairly simple, with most likely just a lightweight AJAX library necessary for requests.
- I can also imagine a world where the front-end turns into more of an app environment with a full framework, i.e. Backbone. If the complexity increased or there were multiple outcomes of the sign in, I might even recommend it to help manage state, errors and possible paths of success. However, some simple Javascript does the trick here, while still keeping things server-side rendered and quick.
- The data for visit logs is stored entirely in a post object, without any meta fields necessary. This is done so as not to bloat the database, but also to make querying more performant and effective. For instance, when checking if a user has signed in on a certain day I can use the WordPress API to query against the title instead of a costly meta query, or needing to create my own SQL. This also results in less query when pulling the data out on the back-end.
- The coworker list is stored in the options table, since it is a static piece of data. Before it is inserted into the database, I've created a new hash map which adds each name as a key. This makes it easier to retrieve a desk number or list of coworkers without a complicated array function to sort through the data.
- There are no options for the plugin, but I've heavily edited the Visit Log archive view in the WordPress admin to allow users to sort data by different fields, and delete individual logs with one click (and undo if necessary).
- Text used throughout the plugin is localized using the 'visitizer' text domain.
- I've tested the plugin back to PHP 5.4, and did not see any errors.