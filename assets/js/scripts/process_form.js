(function($) {
	var vs;
	
	/**
	 * Handle everything in a consistent object to avoid global scope
	 * 
	 * @type {Object}
	 */
	var VisitorForm = {
		
		/**
		 * Keep all the selectors and data in one place to 
		 * easily reference, and reset later
		 * 
		 * @type {Object}
		 */
		settings: {
			form: 			$('#visit_signup_form'),
			errorEl: 		$('#vt_errors'),
			errors: 		false,
			success: 		$('#vt_success'),
			visting: 		$('.js-vt_visiting'),
			desk: 			$('.js-vt_desk'),
			newNonce: 	''
		},
		
		/**
		 * Save elements and data for reference and kickoff
		 * binding
		 */
		init: function() {
			vs = this.settings;
			this.bindActions();
		},
		
		/**
		 * Handle all actions
		 */
		bindActions: function() {
			vs.form.on( 'submit', VisitorForm.submitForm );
		},
		
		/**
		 * Handler for submitted form
		 *
		 * Takes all data from the form, checks to see if any are empty
		 * and passes data to proper validations. If everything checks out,
		 * we can collect the data and submit an AJAX request
		 * 
		 * @param  {Object} e Event object.
		 */
		submitForm: function(e) {
			e.preventDefault();
			
			VisitorForm.clearErrors();
			
			var name 			= $('#vt_name_input').val();
			var email 		= $('#vt_email_input').val();
			var visiting 	= $('#vt_visiting_input').val();
			var nonce 		= $('#vt_visitor_form').val();
			
			if( '' === name ) {
				VisitorForm.addToErrors( VT.messages.empty_name );
			}

			if( '' === email ) {
				VisitorForm.addToErrors( VT.messages.invalid_email );
			}
	 
			if( '' === visiting ) {
				VisitorForm.addToErrors( VT.messages.empty_visiting );
			}
			
			VisitorForm.validateEmail( email );
			
			VisitorForm.showErrors();
			
			if( vs.errors === false ) {
				var data = {
					email: email,
					name: name,
					visiting: visiting,
					nonce: nonce,
					action: 'visitor_signin'
				};				
				VisitorForm.ajaxRequest( data );
			}
			
		},
		
		/**
		 * Wrapper for AJAX request, but also sets opacity of form down
		 * so that user knows the form is begin submitted
		 * 
		 * @param  {Object} data Full data object from form.
		 */
		ajaxRequest: function(data) {
			VisitorForm.clearErrors();
			vs.form.css( 'opacity', '0.4' );
			$.ajax({
				url: VT.ajaxurl,
				method: 'POST',
				data: data,
				success: VisitorForm.onSuccess,
				error: VisitorForm.onError
			});
		},
		
		/**
		 * Handles AJAX request success
		 *
		 * Checks data object, and either adds all errors sent back
		 * to the global error object, then displays them OR 
		 * proccesses the data sent back on true success and then 
		 * displays them, hides the form, and resets the form 
		 * after 3 seconds
		 * 
		 * @param  {Object} data Data sent back by AJAX request.
		 */
		onSuccess: function( data ) {
			var obj = JSON.parse( data );
			if( obj.status === 'error' ) {
				var i, l = obj.errors.length;
				for( i = 0; i < l; i++ ) {
					VisitorForm.addToErrors( obj.errors[i] );
				}
				VisitorForm.showErrors();
				
				vs.form.css('opacity', '1');
			} else {
				var vs_str = obj.visiting;
				vs.visting.text( vs_str );
				
				var dk_str = obj.desk;
				vs.desk.text( dk_str );
				
				vs.newNonce = obj.next_nonce;
								
				vs.form.hide();
				vs.success.show();
		
				setTimeout(function() {
					VisitorForm.resetForm();
				}, 3000);
			}
		},
		
		/**
		 * If there is an error from the server, we let the user know
		 */
		onError: function() {
			VisitorForm.addToErrors( VT.messages.server_issue );
			VisitorForm.showErrors();
		},
		
		/**
		 * Validate email address against regular expression
		 * 
		 * We only check if no other errors exist, so that duplication
		 * does not occur
		 * 
		 * @param  {String} email Email to check.
		 */
		validateEmail: function( email ) {
			if( vs.errors === false ) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;		
				
				if( !re.test(email) ) {
					VisitorForm.addToErrors( VT.messages.invalid_email );
				}
			}
		},
		
		/**
		 * Add an error to the global error variable
		 * 
		 * @param  {String} error Text of error
		 */
		addToErrors: function( error ) {
			if( false === vs.errors ) {
				vs.errors = [];
			}
			vs.errors.push( error );
		},
		
		/**
		 * Clears error alert, used more then once so broken into separate
		 * function
		 */
		clearErrors: function() {
			vs.errors = false;
			vs.errorEl.empty();
			vs.errorEl.hide();
		},
		
		/**
		 * Checks if errors exist, and if they do, add them to the HTML
		 * of the error Element
		 */
		showErrors: function() {
			if( vs.errors ) {
				vs.errorEl.show();
				var i, l = vs.errors.length;
				for( i = 0; i < l; i++ ) {
					var newEl = $('<li>');
					newEl.text( vs.errors[i] );
					vs.errorEl.append(newEl);
				}
			}
		},
		
		/**
		 * Reset the form back to its original state
		 * Clear the form's values and add new nonce from server
		 */
		resetForm: function() {
			VisitorForm.clearErrors();
			
			var newNonce = vs.newNonce;
			vs.form.find('input').not('input[type="submit"]').val('');
			vs.form.find('#vt_visitor_form').val(newNonce);
						
			vs.form.css( 'opacity', '1' );
			vs.form.show();
			vs.form.find('#vt_name_input').focus();
			vs.success.hide();
		}
	};
	
	// Make sure to intialize object when DOM is loaded
	$(document).ready(function() {
		VisitorForm.init();
	});
})(jQuery);