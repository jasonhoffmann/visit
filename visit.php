<?php
/**
 * Visitizer
 *
 * Logs Incoming Visitors and ensures that visitors can only enter
 * once per day.
 *
 * Plugin Name: Visitizer
 * Plugin URI: http://reactivstudios.com
 * Description: Logs incoming visitors and stores them on the backend
 * Version: 0.1
 * Author: Jay Hoffmann
 * Author URI: http://jayhoffmann.com
 * Text Domain: visitizer
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /languages
 *
 * @package VS_Visitzer
 */

	define( 'VT__VERSION',            '0.1' );
	define( 'VT__PLUGIN_DIR',         plugin_dir_path( __FILE__ ) );
	define( 'VT__PLUGIN_URL',         plugin_dir_url( __FILE__ ) );
	define( 'VT__PLUGIN_BASE',        plugin_basename( __FILE__ ) );
	define( 'VT__PLUGIN_FILE',        __FILE__ );

require_once( VT__PLUGIN_DIR . 'includes/class-visit-post-type.php' );
require_once( VT__PLUGIN_DIR . 'includes/class-visit-activation.php' );
require_once( VT__PLUGIN_DIR . 'includes/class-visit-front.php' );

add_action( 'plugins_loaded', array( 'Visit_Post_Type', 'init' ) );
add_action( 'plugins_loaded', array( 'Visit_Front', 'init' ) );

// Only load admin classes on the backend.
if ( is_admin() ) {
	require_once( VT__PLUGIN_DIR . 'includes/class-visit-admin.php' );
	add_action( 'admin_init', array( 'Visit_Admin', 'init' ) );
}

register_activation_hook( VT__PLUGIN_FILE, array( 'VisitActivation', 'activate' ) );
